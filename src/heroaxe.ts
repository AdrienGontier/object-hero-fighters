import { Hero } from "./hero";
import { Weapon } from "./weapon";
import HeroSword from "./herosword";
import HeroSpear from "./herospear";


export default class HeroAxe extends Hero {
    
    constructor(name:string, power:number, life:number) {
        super(name, power, life)
        this.name = name;
        this.power = power;
        this.life = life;
    }
    
    axe = new Weapon("axe");
    
    attack(opponent:Hero) {
        
        if(opponent instanceof HeroSword) {
            opponent.life -= (this.power) * 2;
        } else {
            super.attack(opponent);
        }
        
    }
    
}

let myHeroAxe = new HeroAxe("Super Hero", 20, 100);
let myHeroSpear = new HeroSpear("Super Hero", 10, 100);
let myHeroSword = new HeroSword("Super Hero", 100, 100);


// myHeroSpear.attack(myHeroAxe);

console.log(myHeroAxe);
console.log(myHeroSword);


do {
    
    console.log(myHeroAxe);
    console.log(myHeroSword);
    
    myHeroSpear.attack(myHeroAxe);
    myHeroAxe.attack(myHeroSword);
    
} while ((myHeroAxe.life > 0) && (myHeroSword.life > 0));

if (myHeroAxe.life > 0) {
    console.log(myHeroAxe.name + " " + "wins");
}

if (myHeroSword.life > 0) {
    console.log(myHeroSword.name + " " + "wins");
}

if ((myHeroAxe.life = 0) && (myHeroSword.life = 0)) {
    console.log("It's a draw");
    
}




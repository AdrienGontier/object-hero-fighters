import { Weapon } from "./weapon";

export class Hero {

    private _name : string;
    private _power : number;
    private _life : number;
    private _weapon: Weapon;
    

    constructor(name: string, power: number, life:number) {
        this._name = name;
        this._power = power;
        this._life = life;
        this._weapon = Weapon;
    }

    get name() : string {
        return this._name;
    }

    set name(name : string) {
        this._name = name;
    }

    get power() : number {
        return this._power;
    }

    set power(power : number) {
        this._power = power;
    }

    get life() : number {
        return this._life;
    }

    set life(life : number) {
        this._life = life;
    }

    get weapon() : Weapon {
        return this._weapon;
    }

    set weapon(weapon : Weapon) {
        this._weapon = weapon;
    }

    

    attack(opponent: Hero) {

        opponent.life -= this._power;


        // La méthode `attack` a un paramètre `opponent` (de type `Hero`).
        // Il faut réduire le nombre (`life`) de `opponent` d'autant de dégats (`power`) de l'attaquant.
    }


    isAlive() {

        // La méthode `isAlive` devrait retourner `true`
        // si le nombre de points de vie du héros est supérieur à zéro 
        // et `false` sinon.

        if(this._life > 0) {
            return true;
        } else {
            return false;
        }
    }


// ​

// Crée deux instances de `Hero` et vérifie que les méthodes `attack` et `isAlive` fonctionnent.

}

// let moi = new Hero("Adrien", 100, 100);

// let nemesis = new Hero("Pierre", 50, 50)

// nemesis.attack(moi);

// // moi.isAlive(moi.life);
// // nemesis.isAlive(nemesis.life);

// console.log(moi);
// console.log(nemesis);
// console.log(moi.isAlive());
// console.log(nemesis.isAlive());



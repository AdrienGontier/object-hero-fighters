import { Hero } from "./hero";
import { Weapon } from "./weapon";
import HeroAxe from "./herosword";
import HeroSword from "./herospear";


export default class HeroSpear extends Hero {

    constructor(name:string, power:number, life:number) {
        super(name, power, life)
        this.name = name;
        this.power = power;
        this.life = life;
    }

    axe = new Weapon("spear");

    attack(opponent:Hero) {

        if(opponent instanceof HeroAxe) {
            opponent.life -= (this.power) * 2;
        } else {
            super.attack(opponent);
        }
        
    }

}

// let myHeroAxe = new HeroAxe("Super Hero", 100, 100);
// let myHeroSpear = new HeroSpear("Super Hero", 100, 100);
// let myHeroSword = new HeroSword("Super Hero", 100, 100);


// myHeroSpear.attack(myHeroAxe);

// console.log(myHeroAxe);





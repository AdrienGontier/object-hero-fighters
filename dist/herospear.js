"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const hero_1 = require("./hero");
const weapon_1 = require("./weapon");
const herosword_1 = __importDefault(require("./herosword"));
class HeroSpear extends hero_1.Hero {
    constructor(name, power, life) {
        super(name, power, life);
        this.axe = new weapon_1.Weapon("spear");
        this.name = name;
        this.power = power;
        this.life = life;
    }
    attack(opponent) {
        if (opponent instanceof herosword_1.default) {
            opponent.life -= (this.power) * 2;
        }
        else {
            super.attack(opponent);
        }
    }
}
exports.default = HeroSpear;
// let myHeroAxe = new HeroAxe("Super Hero", 100, 100);
// let myHeroSpear = new HeroSpear("Super Hero", 100, 100);
// let myHeroSword = new HeroSword("Super Hero", 100, 100);
// myHeroSpear.attack(myHeroAxe);
// console.log(myHeroAxe);

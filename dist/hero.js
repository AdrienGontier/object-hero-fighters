"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Hero = void 0;
const weapon_1 = require("./weapon");
class Hero {
    constructor(name, power, life) {
        this._name = name;
        this._power = power;
        this._life = life;
        this._weapon = weapon_1.Weapon;
    }
    get name() {
        return this._name;
    }
    set name(name) {
        this._name = name;
    }
    get power() {
        return this._power;
    }
    set power(power) {
        this._power = power;
    }
    get life() {
        return this._life;
    }
    set life(life) {
        this._life = life;
    }
    get weapon() {
        return this._weapon;
    }
    set weapon(weapon) {
        this._weapon = weapon;
    }
    attack(opponent) {
        opponent.life -= this._power;
        // La méthode `attack` a un paramètre `opponent` (de type `Hero`).
        // Il faut réduire le nombre (`life`) de `opponent` d'autant de dégats (`power`) de l'attaquant.
    }
    isAlive() {
        // La méthode `isAlive` devrait retourner `true`
        // si le nombre de points de vie du héros est supérieur à zéro 
        // et `false` sinon.
        if (this._life > 0) {
            return true;
        }
        else {
            return false;
        }
    }
}
exports.Hero = Hero;
// let moi = new Hero("Adrien", 100, 100);
// let nemesis = new Hero("Pierre", 50, 50)
// nemesis.attack(moi);
// // moi.isAlive(moi.life);
// // nemesis.isAlive(nemesis.life);
// console.log(moi);
// console.log(nemesis);
// console.log(moi.isAlive());
// console.log(nemesis.isAlive());

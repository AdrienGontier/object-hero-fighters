"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const hero_1 = require("./hero");
const weapon_1 = require("./weapon");
const herosword_1 = __importDefault(require("./herosword"));
const herospear_1 = __importDefault(require("./herospear"));
class HeroAxe extends hero_1.Hero {
    constructor(name, power, life) {
        super(name, power, life);
        this.axe = new weapon_1.Weapon("axe");
        this.name = name;
        this.power = power;
        this.life = life;
    }
    attack(opponent) {
        if (opponent instanceof herosword_1.default) {
            opponent.life -= (this.power) * 2;
        }
        else {
            super.attack(opponent);
        }
    }
}
exports.default = HeroAxe;
let myHeroAxe = new HeroAxe("Super Hero", 20, 100);
let myHeroSpear = new herospear_1.default("Super Hero", 10, 100);
let myHeroSword = new herosword_1.default("Super Hero", 100, 100);
// myHeroSpear.attack(myHeroAxe);
console.log(myHeroAxe);
console.log(myHeroSword);
do {
    console.log(myHeroAxe);
    console.log(myHeroSword);
    myHeroSpear.attack(myHeroAxe);
    myHeroAxe.attack(myHeroSword);
} while ((myHeroAxe.life > 0) && (myHeroSword.life > 0));
if (myHeroAxe.life > 0) {
    console.log(myHeroAxe.name + " " + "wins");
}
if (myHeroSword.life > 0) {
    console.log(myHeroSword.name + " " + "wins");
}
if ((myHeroAxe.life = 0) && (myHeroSword.life = 0)) {
    console.log("It's a draw");
}
